<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Wireframe</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">  

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
     <link href="css/style.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
   <?php include ('header.php'); ?>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img style="object-fit:cover;" class="first-slide" src="img/land7.png" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
            </div>
          </div>
        </div>
        <div class="item">
          <img style="object-fit:cover;" class="second-slide" src="img/land6.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
            </div>
          </div>
        </div>
        <div class="item">
          <img style="object-fit:cover;" class="third-slide" src="img/land2.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    <!-- Articles Fall Here
    ================================================== -->
  

  <div class="container" style="background-color: white">
    <div class="row">                   
  
  <p class="text-center" style="font-weight: bold;padding-top: 5px;">content</p>
  <?php
                    $limit_word = 200;
                    $con=mysqli_connect("localhost","root","","articles");
                    if (mysqli_connect_errno()){echo "Failed to connect to MySQL: " . mysqli_connect_error();}
                    $query = mysqli_query($con, "SELECT * FROM posts ORDER BY posts.reg_date DESC LIMIT 6");
                    $result = $query;
                    $num_results = mysqli_num_rows($result);
                    for($i = 0; $i < $num_results; $i++)
                    {
                    $row = mysqli_fetch_array($result);
                    $data1 = $row['comment'];
                    $id = $row['id'];
                    $data2 = substr( $data1,0,$limit_word);
                    echo'
    <div class="col-xs-12 col-sm-12 col-md-6">  
       <div class="thumbnail">
               
                <div class="caption">     
    
                
                    <h4 class="text-left">'.$row['title'].'</h4>
                     
                    <p>Posted on '.$row['reg_date'].'</p>
                    <p class="items">
                      '.$data2.'
     <a href="articles.php?id='.$row['id'].'">..Read More</a>
                         <span><a href="#" style="color: black; text-decoration: underline;"></a></span>
                    </p>
                </div>
            </div>
        </div> ';
    }

?>     
</div>
    </div><!-- /.container -->
<!---->
      <!-- FOOTER -->
     <?php include("footer.php") ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--<script>window.jQuery || document.write("<script src="../../assets/js/vendor/jquery.min.js"><\/script>")</script>-->
    <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/script.js"></script>-->
  </body>
</html>

