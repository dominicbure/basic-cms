<?php
session_start();
 require('classes/config.class.php');
 $config = new Config();

require('classes/connect.class.php');
$connect = new Connect();  

require('classes/auth.class.php');
$auth = new Auth();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<?php
if (isset($_POST["username"]) && isset($_POST["password"]))
if ($_POST["username"] != "" && $_POST["password"] != "") {
    
    $connect->login($_POST["username"],$_POST["password"]);
    
    if(@$_POST["username"] != $username || $_POST["password"] != $password) {
        
        echo  "<script type='text/javascript'>alert('Please Enter Correct Credentials')</script>";
        
    }
}else {
    
    echo "<script type='text/javascript'>alert('Please Enter Username and Password')</script>";
 }
 
 if (!isset($_SESSION['username'])) {
     
     include('login.php');
 }
 
 if (isset($_SESSION['username'])) {
     
     include ('admin-view.php');
     
}
?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>