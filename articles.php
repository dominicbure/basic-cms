<?php
include ('classes/edit.class.php');
$edit = new Edit();
$edit->connect();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Articles</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
     <link href="css/style.css" rel="stylesheet">   
    <!-- Custom styles for this template -->  
    </head>  
  <body>
       <?php include ("header.php") ?>
       <ol class="breadcrumb">
  <li><a href="index.php">Home</a></li>
  <li><a href="about-us.php">About Us</a></li>
  <li class="active">Articles</li>
  <li><a href="contact-us.php">Contact Us</a></li>
</ol>

   <div class="container" style="background-color:white;">

     <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">The  Blog</h1>
        <p class="lead blog-description">The official  Blog.</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title"><?php echo $edit->title; ?></h2>
            <p class="blog-post-meta">Posted on <span><?php echo $edit->datecreated; ?></span></p>

            <p><?php echo $edit->article;?></p>
            </div><!-- /.blog-post -->

        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module">
            <h4>Archives</h4>
<?php
$con=mysqli_connect("localhost","root","","articles");

if (mysqli_connect_errno()) {
  
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  
}

$query = mysqli_query($con, "SELECT * FROM posts ORDER BY posts.reg_date DESC");
$result = $query;
$num_results = mysqli_num_rows($result);

for($i = 0; $i < $num_results; $i++) {
  
  $row = mysqli_fetch_array($result);
  
  echo'
          
            <ol class="list-unstyled">
            
              <li><a href="articles.php?id='.$row['id'].'">'.$row['title'].'</a></li>
            </ol>';
}

?>
          </div>
         
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

     <?php include ("footer.php") ?>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
  <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/script.js"></script>
</html>