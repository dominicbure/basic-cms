<?php
session_start();
require('access-control.php');

require('classes/session-timeout.class.php');
$session = new Start_session();
$session->start_session();
include ('classes/change_password.class2.php');
$password = new Password();
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Change Password</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">  

    <!-- Custom styles for this template -->
    <!--<link href="css/carousel.css" rel="stylesheet">
     <link href="css/style.css" rel="stylesheet">-->
</head>
<body>
<div class="container">
<form name="frmChange" class="form-signin" action='#' method="POST" onSubmit="return validatePassword()">
  <fieldset>
    <div id="legend">
      <legend class="">Change Password  -  <?php echo $_SESSION['username']; ?></legend>
    </div>
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">Current Password</label>
      <div class="controls">
        <input type="password" id="currentPassword" name="currentPassword" placeholder="" class="input-xlarge" class="required">
        <p class="help-block">Password should be at least 4 characters</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">New Password</label>
      <div class="controls">
        <input type="password" id="newPassword" name="newPassword" placeholder="" class="input-xlarge" class="required">
        <p class="help-block">Password should be at least 4 characters</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password -->
      <label class="control-label"  for="password_confirm">Password (Confirm)</label>
      <div class="controls">
        <input type="password" id="confirmPassword" name="confirmPassword" placeholder="" class="input-xlarge" class="required">
        <p class="help-block">Please confirm password</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Button -->
      <div class="controls">
        <button type="submit" name="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
  </fieldset>
</form>
</div>

</body>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/script.js"></script>
</html>