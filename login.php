<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Signin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

   
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
    </head>

  <body>

    <div class="container">

      <form class="form-signin" action="#" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputName" class="sr-only">User Name</label>
        <input type="text" name="username" id="inputName" class="form-control" placeholder="User Name" >
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" >
        <button class="btn btn-lg btn-primary" type="submit">Sign in</button>
        <a href="change-password.php">Change Password</a>
        </div>
        
      </form>

    </div> <!-- /container -->
  </body>
</html>