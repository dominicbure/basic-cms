<?php
$uid = isset($_POST['username']) ? $_POST['username'] : $_SESSION['username'];
$pwd = isset($_POST['password']) ? $_POST['password'] : $_SESSION['password'];

if(!isset($uid)) {
  ?>
  <!DOCTYPE html PUBLIC "-//W3C/DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title> Please Log In for Access </title>
    <meta http-equiv="Content-Type"
      content="text/html; charset=iso-8859-1" />
  </head>
  <body>
  <h1> Login Required </h1>
  <p>You must log in to access this area of the site.<a href="admin-index.php">click here</a>
     to sign in.</p>
  </body>
  </html>
  <?php
  exit;
}

?>

