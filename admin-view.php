<?php
session_start();
require('access-control.php');

require('classes/create.class.php');

$create = new Create();

require('classes/config.class.php');
$config = new Config();

require('classes/connect.class.php');
$connect = new Connect();  

require('classes/session-timeout.class.php');
$session = new Start_session();
$session->start_session();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Admin Page</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"> 
 <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/table.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
 <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href=""></a>
        </div>
        
        <ul class="nav navbar-nav navbar-right">
        
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                class="glyphicon glyphicon-user"></span><?php
           if(isset($_SESSION["username"])) { ?><?php echo $_SESSION["username"]; ?>
                <?php
} ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <!--<li><a href="#"><span class="glyphicon glyphicon-user"></span>Profile</a></li>-->
                    <li><a href="change-password2.php"><span class="glyphicon glyphicon-cog"></span>change Password</a></li>
                    <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span>Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <div class="row">
    
    <p></p>
    <h1 style="text-align:center">Articles Posted</h1>    
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Articles</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <button type="button" class="btn btn-sm btn-primary btn-create" data-toggle="modal" data-target="#contact" data-original-title>Create New</button>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        <th><em class="fa fa-cog"></em></th>
                        <th class="hidden-xs">ID</th>
                        <th>Title</th>
                        <th>Comment</th>
                        <th>Date Posted</th>
                    </tr> 
                  </thead>
                  
                          
 
 <?php
$con=mysqli_connect("localhost","root","","articles");

if (mysqli_connect_errno()) {echo "Failed to connect to MySQL: " . mysqli_connect_error();

}

$query = mysqli_query($con, "SELECT * FROM posts ORDER BY posts.reg_date DESC");
$result = $query;
$num_results = mysqli_num_rows($result);

for($i = 0; $i < $num_results; $i++) {
    
    $row = mysqli_fetch_array($result);
    
    echo'
 <tbody>
<tr>
                            <td align="center">
                              <a class="btn btn-default" href="edit.php?id='.$row['id'].'"><em class="fa fa-pencil"></em></a>
                              <a onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-danger" href="delete.class.php?id='.$row['id'].'"><em class="fa fa-trash"></em></a>
                            </td>

                            <td class="hidden-xs">'.$row['id'].'</td>
                            <td>'.$row['title'].'</td>
                            <td>'.$row['comment'].'</td>
                            <td>'.$row['reg_date'].'</td>
                          </tr>
                        </tbody>';
}

?>
                </table>
            
              </div>
              <div class="panel-footer">
                <div class="row">
                  <div class="col col-xs-4">
                  </div>
                  <div class="col col-xs-8">
                   
                    <ul class="pagination visible-xs pull-right">
                      
                    </ul>
                  </div>
                </div>
              </div>
            </div>
</div></div></div>
<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-info-sign"></span> Please Fill in</h4>
                    </div>
                    <form action="#" method="POST" accept-charset="utf-8">
                    <div class="modal-body" style="padding: 5px;">
                          <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                    <input class="form-control" name="title" placeholder="Title" type="text" required autofocus />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <textarea style="resize:vertical;" class="form-control" name="bodytext" placeholder="Write Article Here..." rows="15" name="comment" required></textarea>
                                </div>
                            </div>
                        </div>  
                        <div class="panel-footer" style="margin-bottom:-14px;">
                            <input type="submit" class="btn btn-success" value="Save Article"/>
                                <!--<span class="glyphicon glyphicon-ok"></span>-->
                            <input type="reset" class="btn btn-danger" value="Clear" />
                                <!--<span class="glyphicon glyphicon-remove"></span>-->
                            <button style="float: right;" type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</body>
 <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/script.js"></script>
</html>