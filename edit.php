<?php
session_start();
require('classes/session-timeout.class.php');
$session = new Start_session();
$session->start_session();
require('access-control.php');
include ('classes/edit.class.php');
$edit = new Edit();
$edit->update_article();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Edit Page</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"> 
 <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css" rel="stylesheet" type="text/css">
</head>
<body>
  <div class="container">
  <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href=""></a>
        </div>
        
        <ul class="nav navbar-nav navbar-right">
        
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                class="glyphicon glyphicon-user"></span><?php
           if(isset($_SESSION["username"])) { ?><?php echo $_SESSION["username"]; ?>
                <?php
} ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <!--<li><a href="#"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-cog"></span>Settings</a></li>-->
                    <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span>Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
<form action="#" method="POST" accept-charset="utf-8">
  <div class="form-group">
    <label for="InputTitle">Title</label>
    <input type="text" class="form-control"  name="title" value="<?php echo $edit->title; ?>" placeholder="Edit Title">
  </div>
  <div class="form-group">
    <label for="Text">Article</label>
    <textarea   name="comment" class="form-control"  placeholder="Edit Article Here..." rows ="10" cols ="1"><?php echo $edit->article;?></textarea>
  </div>
  <button type="submit" name="btn-update" class="btn btn-primary">Update</button>
 <button type="submit" class="btn btn-primary"><a href="admin-view.php" style="color:white;">Return to admin Page</a></button>
</form>
</div>
</body>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/script.js"></script>
</html>