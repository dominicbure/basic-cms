<?php
class Start_session{
    
    function start_session() {
        
        $now = time();
        
        if (isset($_SESSION['discard_after']) && $now > $_SESSION['discard_after']) {
            
            // this session has worn out its welcome; kill it and start a brand new one
            session_unset();
            session_destroy();
            echo "<script>alert('session timed out, please login to resume'); window.location.href='".$url." admin-index.php';</script>";
            
        }
        
        // either new or old, it should live at most for another hour
        $_SESSION['discard_after'] = $now + 1800;

    }
}
    ?>