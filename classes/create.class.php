<?php
class Create{

    function Create() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "articles";
    
    try {
         $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
         if(isset($_POST["title"]) && isset($_POST["bodytext"])){
          $title = htmlentities($_POST["title"]);
         $comment =htmlentities($_POST["bodytext"]);
         // set the PDO error mode to exception
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         $stmt = $conn->prepare ("INSERT INTO posts (title, comment) VALUES (?, ?)");
         // use exec() because no results are returned
         $stmt->bindParam(1, $title);
         $stmt->bindParam(2, $comment);
         $stmt->execute();
         
         if($title !="" && $comment !="") {
             //$conn->exec($insert_sql);
             echo "<script>alert(New record created successfully)</script>";
             //echo $insert_sql;
             }
             }
    } catch(PDOException $e) {
                 echo 'Error '  .$e->getMessage();
                 }
             $conn = null;
             }
      }
?> 