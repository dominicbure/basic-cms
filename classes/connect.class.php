  <?php
  
      class Connect {
          
          var $conn;
          var $config;
          
          // This is a constructor for this. Will run everything inside when class is created.

          function Connect() {
              
              $this->config = new Config();
              $conn = $this->connect_to_db();
          }
          
          function connect_to_db() {
              
              $servername = $this->config->servername;
              $username = $this->config->username;
              $password = $this->config->password;
              $dbname = $this->config->dbname;
              
              try {
                  
                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                  
                     // set the PDO error mode to exception
                  
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                  if ($this->config->debug) 
                   // echo "Connected successfully<br>";
                
                return $conn;
                }catch(PDOException $e) {
                       echo "Connection failed: " . $e->getMessage();
                       }
                       
         }
         
         function login($username, $password) {
             
              try {
                   $conn = $this->connect_to_db();
                   $password =  md5($password);
                                      
                    $username_sql = $conn->prepare("SELECT username FROM users WHERE username=?");
                    $username_sql->bindParam(1, $username);
                    $username_sql->execute();
                
                    $password_sql = $conn->prepare("SELECT * FROM users WHERE username=? and password=?");
                    $password_sql->bindParam(1, $username);
                    $password_sql->bindParam(2, $password);
                    $password_sql->execute();
                    
                    for($i=0; $row = $password_sql->fetch(); $i++) {                        

                        session_start();    

                        $_SESSION['username'] = $username;
                        $_SESSION['password'] = $password;

                        header('Location: admin-view.php');

                        // header("Location: admin-view.php");
                        // exit();
                    }
                  } catch (PDOException $e) {
                      
                      header("Location: ". $url . "admin-view.php");
                      
                      echo "Connection failed: " . $e->getMessage();
                    }
         }
      }
?>
